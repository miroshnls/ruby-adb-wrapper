#Wrapper for android adb commands
class Wrapper
	def get_devices
		output = `adb devices`
		output =  output.split("\n").select{ |line| line.include? "\tdevice"} 
		devices = output.each{ |line| print line.chomp!("\tdevice") + "\n"}
		return devices
	end
	
	def install(package, device = nil, *options)
		adb("install", package, device, *options)
	end

	def uninstall(package, device = nil, *options)
		adb("uninstall", package, device, *options)
	end

	private
	def adb(command, package, device = nil, *options)
		if device
			str = "adb -s #{device} #{command} #{options.join(' ')} #{package}"
		else str = "adb #{command} #{options.join(' ')} #{package}"
		end
		output = `#{str}`
		if output.downcase.include? "fail"
			raise "Error: #{output}"
		end
	end
end